# Final GSoC Report for Android SDK Tools in Debian

I worked on Packaging Android SDK Tools this summer as a part of GSoC.

Link to GSoC Project Page: https://summerofcode.withgoogle.com/projects/#6072759591895040

Link to DebianWiki Page of Project: https://wiki.debian.org/AndroidTools


It is time to write my final report for Google Summer Of Code 2018 as it is coming to an end.
This was an awesome experience. Not only did I learn so much on the technical side but I also learnt how to operate in open source communities and contribute in an orderd way.
I had pleasure to know some cool and knowledgeable people in the role of my mentors who were always available to help me.

## Working with Debian

Way back in March month of this year, I was just a regular user of Debian Linux but now I know how Debian Packaging, Bug Tracking and Debian community in general works, making Debian an incredible project it is. 

## Work I have done

My project was to package and update existing Android SDK packages to latest upstream versions. I have worked on several repositories in GSoC period. Below is the commits I have made in different repos.


# Commits merged in main project repos:

## [android-platform-external-libunwind](https://salsa.debian.org/android-tools-team/android-platform-external-libunwind)

- [Update debian/changelog][1]
- [New upstream version 8.1.0+r23][2]
- [Add description for patch : 20150704-CVE-2015-3239_dwarf_i.h][3]
- [Add myself to Uploaders][4]
- [Remove non-existing entries from copyright][5]
- [Update Standards-Version][6]
- [Update Vcs links][7]
- [Fix FTCBFS (Closes: #897062)][8]
- [Update debian/changelog to list latest changes][9]

## [android-platform-external-libselinux](https://salsa.debian.org/android-tools-team/android-platform-external-libselinux)

- [Include libsepol headers][10]
- [Build libsepol required by fastboot][11]
- [Update debian/changelog][12]
- [Update libselinux.mk][13]
- [Update debian/rules and debian/control to incorporate libsepol][14]
- [New patch: Fix header path][15]
- [Fixes to remove lintian warnings][16]
- [Fix install files][17]
- [Add myself to uploaders][18]
- [Fix duplicate description][19]
- [Use debian/clean to simplify build scripts][20]
- [Update debian/changelog to list latest changes][21]

## [android-platform-system-core](https://salsa.debian.org/android-tools-team/android-platform-system-core)

- [Update debian/changelog][22]
- [New upstream version 8.1.0+r23][23]
- [Update libcutils patch][24]
- [Update cutils_atomic patch][25]
- [Remove patch : fs_prepare_path_impl_open_mode_missing ][26]
- [Remove patch: fix-CVE-2017-0647.patch][27]
- [Remove patches: adb_libssl_11.diff & adb_libssl_bc.diff][28]
- [Update patch: move-log-file-to-proper-dir][29]
- [New patch: added missing headers][30]
- [Update liblog.mk][31]
- [Update libcutils.mk][32]
- [Update liblog.mk for cpp file compile rules][33]
- [Update libcutils.mk][34]
- [New patch : Direct include fs_config header][35]
- [Sync libadb.mk with upstream Android.mk][36]
- [Update libadb.mk][37]
- [Update build dependencies][38]
- [Update patch: Added missing headers & update adb.mk][39]
- [Update patch : direct include header][40]
- [New patch : fix-header-path for libusb.h][41]
- [Update libadb.mk for libusb linker][42]
- [Build libcrypto_utils.so][43]
- [Update libadb.mk and adb.mk][44]
- [Update makefiles][45]
- [New patch added & deleted obsolete patch][46]
- [Update ziparchive-dev install file][47]
- [Add install file for libcrypto_utils][48]
- [Update libcrypto_utils.mk][49]
- [Changes to install libcrypto_utils library][50]
- [Added myself to uploaders][51]
- [Update build dependencies][52]
- [Update debian/rules][53]
- [Build libdiagnoseusb library required by adb and fastboot][54]
- [Update rules and control files to include libdiagnoseusb][55]
- [link libdiagnoseusb in adb.mk][56]
- [Update debian/rules][57]
- [Fix typo in libutils.mk][58]
- [Update libbase.mk sources][59]
- [Update makefiles][60]
- [Add libdemangle sources in libbacktrace][61]
- [Update header paths in install files][62]
- [Remove obsolete patchs][63]
- [Update gitlab-ci.yml to include boringssl][64]

## [android-platform-framework-base](https://salsa.debian.org/android-tools-team/android-platform-frameworks-base)

- [Update makefiles][65]
- [Add clean file][66]
- [New patch to fix proto import errors][67]
- [New patch: escape-apostrophe][68]
- [Switch to aapt2 for framework-res.apk generation][69]
- [Update header path in libandroidfw install][70]
- [Add newline at the end in debian/patches/series to remove lintian warning][71]
- [Update debian/changelog][72]
- [Add gitlab-ci.yml][73]
- [Update debian/rules][74]
  

# Commits not yet mergrd in main project repos:

## [android-platform-system-tools-aidl](https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl)

- [Fix typo in changelog][75]
- [Update makefiles][76]
- [Update debian/rules][77]
- [Update install file][78]
- [Add debian/clean file][79]
- [Update debian/changelog][80]
- [Add gitlab-ci file][81]

# Deliverables

Out of the packages I have worked on, following packages have been uploaded Debian testing repos and can be installed using apt package manager:
- android-platform-external-liblinux
- android-platform-external-libunwind

# What is the current state of the project?

Some packages on which I have worked are not yet uploaded because they depend on a new package named 'android-platform-external-boringssl' which is still in [Debian NewQueue](https://wiki.debian.org/NewQueue) and will take some time to come in repos. Once it is available in repos,other packages can also be uploaded and make available to install for end users.

At the time of writing this report, I am currently working on packaging android.jar. Android.jar file contains the public API of the android framework and it is used to compile android apps. It depends on more than 50 source repos of AOSP source so I haven't been able to package it till now. I will be working towards packaging this even after GSoC is over.


# What is next?

As I said above, I will complete my work on packaging android.jar. Even after it is done, I will continue working on this project for as long as I can. We have discussed with our mentors and they have shown great interest in guiding us even after GSoC is over.

In addition to packaging, I am planning to update [wiki page of AndroidTools](https://wiki.debian.org/AndroidTools/) on Debian wiki to reflect changes we have made and state of the project.


[1]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/b2816e105de56cd5bae934e7911c763c2ae3c156
[2]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/c62f11e25ed90a2e5f806beb4fb8aa928c285f57
[3]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/20ec3997dc525b3bfb71a64f91df8dee7356cf16
[4]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/f819e5d303583bf05895205eea42afe7a78e3882
[5]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/e4253bed4b838ecf987a3f976b70b2dd4fd0c5a0
[6]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/4b1682961dc8e03267fe2b9d28f86c82939ac384
[7]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/6ee43364b2242acf8d50ff5ca99424bcfe7d157d
[8]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/59f2bedbddab0781ff784bb4dfb66eddef617e2e
[9]: https://salsa.debian.org/android-tools-team/android-platform-external-libunwind/commit/6f63261c0eb7aaae3f7ff2abd50f167354ae06cf
[10]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/af105211401d5b926d0df48c466844139e6355cc
[11]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/6b36240e81ecdda9da4698d0a9afd4d0ef06616d
[12]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/cea83ce3a40affb40953a0ee7daaf7264bc1e263
[13]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/882093ad47e073365277030eb0add5170c9d57f5
[14]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/57d2610d2c777fb898f25a49aa34df1712fc9224
[15]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/3158e08ad7c3c60d878a1f51fc5911a6f409e7aa
[16]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/ffdb5e67bff09b31e2d0e177b05658ec3af63734
[17]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/911140fd1df861f4781ce3c9dfadb3188415f5bb
[18]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/092a8bcc7c2504c65277e24756dcf315b24d6db2
[19]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/895e537f1b6416916d7a8c38650383f7fcfea23c
[20]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/d96c6a8dffa350d18aed1be6de00178194818357
[21]: https://salsa.debian.org/android-tools-team/android-platform-external-libselinux/commit/a9857473b59835408d6fee666b9333aa41ca34cb
[22]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/1faf2eedd5fd5c8c5cea4d112dd321f92a3c6ad9
[23]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/888d9fb7a23c7dce4d33b87746ce46e9ce36a72c
[24]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/f0b9f393079f5fa5f2ad3371db4aab6250d132c6
[25]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/62df829846f82f4efd91567ee37be5f0a19849f2
[26]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/d81c0c802cc826b53f527c25a7dcb4a129df9810
[27]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/5c4d5fbf8f783386d9f9da516215d4c0acf2f749
[28]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/44c7929c35cd7b29eb2583598424949b5c514d27
[29]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/02090eec7ad1ec54b045ea3b28cc89edba8a2fff
[30]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/39c1052abac7cfae986284573b6e6413cbc13fac
[31]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/640d1f1032aa8f6ca5606a109e8b94ba0b678d6b
[32]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/640d1f1032aa8f6ca5606a109e8b94ba0b678d6b
[33]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/b3477ac850277bded822ec751b87dad2e155e032
[34]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/4a14416b775b68e270ff82e6d25202e2f696a73b
[35]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/776c52b29cca9ae00aa0e3bf785fd763c2b2a4d1
[36]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/c18c5743bdfa8e357720f25dba874d08f763e328
[37]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/f07f5793f3e66affa962ac757924e7d84e2009b7
[38]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/a6b792eabc48b7fbffc1d24d9c6bc7d54bf9baa0
[39]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/b9b7ebab07276427321ae8347e92191a1c67f571
[40]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/f842347fd1130056a6ba786ea339d51bc3b547aa
[41]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/64d38789b39383cd4258d1507cbf1cf7f9b03ae0
[42]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/d601cf34c7f90c4d2ea784a7d854aefa13b40d02
[43]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/8e874844fc996ed14d8e25be74afadbe20a5e2cd
[44]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/f8b900fcb05336e0f66f19a118630c0d40a61c87
[45]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/89fc6046de299b6e639977116f0a4c24b40f8c49
[46]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/17ea984d7a03a4c2c9b6fc6f9f79934afce03630
[47]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/a5c84f17fa0f88cf6d9d0a9fd141f881e22be7df
[48]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/4c1f07fb71f92bcf3c66db1b6f964e6f194df004
[49]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/17b8b29b5a1112f690ed963adf3fea24e60dc8f2
[50]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/fde1df3018b80eee59eec3b6095589c53edbe595
[51]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/880301bf9dc9ec4bf13216ba334f2df0e4d891be
[52]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/99e2524332d4819bebb4038e1ccd102e5ad99be6
[53]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/02a849e44590a8e81866f0d1b232012c3e350dc1
[54]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/e16903376c33e2a137ce80af551a22a9b0e48dab
[55]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/d39733cddcc0ec5d6105708be4fce03ebc135ac7
[56]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/8896377e60e37be1b79b7e014bc254d070696958
[57]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/2853f6be970596fe0e52d0ad999be649043a4fc4
[58]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/7a653c6538f810e71ec019e435a0fe042d07c4e9
[59]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/7fe9c1e639e44f15e4563292ce5e2095e5d4cb4d
[60]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/30a3d1eb2ae47d3ba8862903d013b9e180d12382
[61]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/0b35097dd4ea17109dbdc3bcaf6a8de071fcbd95
[62]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/4911fa076d428aed82d08a173cc52f011b0fb55c
[63]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/aaad68ebf7226c54caa38dc621bc7acccabba8e9
[64]: https://salsa.debian.org/android-tools-team/android-platform-system-core/commit/54ea0735be89bee91dfa0015d9e4f02053b2348e
[65]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/8dac6e7391a99bf4e9d17311c43cbe1fb845002c
[66]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/03776b1c4940ee0f5528f472748e9bca5d8a8567
[67]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/2add3734c5dac5a99686115fc565ee94b068d3a2
[68]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/ebd273bf555b7151c1fa517208b7d446c8a6d6b4
[69]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/70e89079d0c0d7ade06507b8552901c44c41bda1
[70]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/a6a8160949553eb1be2442e5372d3452f506b4ef
[71]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/d754660a65b66731a4c920ea34fab0f95d0526ed
[72]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/cb1a5e46809c2b4470076c7f862da31e0eef275d
[73]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/a53436fb2971d641a3837b421a25c3d972c1dc5f
[74]: https://salsa.debian.org/android-tools-team/android-platform-frameworks-base/commit/27eba3a1e0032202b2e32fcadb442008ab20ec95
[75]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/08d008e4ca1a06cde1303e79eee2cf6a88a5c65b
[76]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/cb73a67495a371210cfe8c14831874e939175035
[77]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/b6edabff2b9248a1776b00712c332d6cd85b1e12
[78]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/2bb484218ecb6a4f6afd1adde0a5e0e98715807b
[79]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/5e09abc7d025e26df2ceef1f9bea43cdf9b13996
[80]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/3614c3165615864b7e71dd0f9ceb6f43afb9c1a6
[81]: https://salsa.debian.org/darkLord-guest/android-platform-system-tools-aidl/commit/9b05fd04aa0798c99bbc47c34041f4366da8cfe1