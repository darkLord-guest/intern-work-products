# Google Summer of code 2018

- **Organization** - [Debian](https://debian.org)
- **Mentors** - [Dashamir Hoxha](https://github.com/dashohoxha) , [Akash Shende](https://github.com/akash0x53)
- **Project Abstract** - [Link](https://summerofcode.withgoogle.com/projects/#5506177505427456)
- **Github repository** - [Virtual LTSP Server](https://github.com/docker-scripts/dev--LTSP)

## Table of contents
1. [Introduction](https://gist.github.com/d78ui98/138c986dffc4d7a094e3ec1c63b545ba#introduction)
1. [Deliverables](https://gist.github.com/d78ui98/138c986dffc4d7a094e3ec1c63b545ba#deliverables)
1. [Weekly reports / Blog posts](https://gist.github.com/d78ui98/138c986dffc4d7a094e3ec1c63b545ba#weekly-reports--blog-posts)
1. [Other contributions](https://gist.github.com/d78ui98/138c986dffc4d7a094e3ec1c63b545ba#other-contributions)

## Introduction
Virtual LTSP server project automates installation and configuration of LTSP server with vagrant. It is the most easiest way to create LTSP setup. We have devloped the project to do the same for linux mint 19 and debian 9. We also also created several scripts for testing, create ltsp client, manage accounts, etc. Also created packer scripts to create vagrant boxes that we will use in the project.



## Deliverables

- Virtual LTSP Server - https://github.com/docker-scripts/dev--LTSP

There are around 50 big and small pull requests and issues. So I am just providing links here this way.
- Pull request made - https://github.com/docker-scripts/dev--LTSP/pulls?utf8=%E2%9C%93&q=user%3Ad78ui98
- Commits made 
  - bionic branch - https://github.com/docker-scripts/dev--LTSP/commits/bionic
  - buster branch - https://github.com/docker-scripts/dev--LTSP/commits/buster
- Issues worked on - https://github.com/docker-scripts/dev--LTSP/issues?utf8=%E2%9C%93&q=user%3Ad78ui98
- Packer scripts to create vagrant box - https://github.com/docker-scripts/dev--LTSP/tree/bionic/packer
- Linux mint tara vagrant box - https://app.vagrantup.com/d78ui98/boxes/linuxmint-19-xfce-32bit
- Wiki pages - https://github.com/docker-scripts/dev--LTSP/wiki

## Weekly reports / Blog posts
- report1:  https://lists.debian.org/debian-outreach/2018/05/msg00020.html


- report2:  https://lists.debian.org/debian-outreach/2018/05/msg00050.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-2-google-summer-of-code-with-debian-9c32d2248d63


- report3:  https://lists.debian.org/debian-outreach/2018/06/msg00015.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-3-google-summer-of-code-with-debian-474a7834bd6d


- report4:  https://lists.debian.org/debian-outreach/2018/06/msg00056.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-4-google-summer-of-code-with-debian-b4446bf65e2e


- report5:  https://lists.debian.org/debian-outreach/2018/06/msg00086.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-5-google-summer-of-code-with-debian-efd8e9c8da82


- report6:  https://lists.debian.org/debian-outreach/2018/06/msg00120.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-6-google-summer-of-code-with-debian-9008a901c6de


- report7:  https://lists.debian.org/debian-outreach/2018/07/msg00000.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-7-google-summer-of-code-with-debian-dd545aba4ed0


- report8:  https://lists.debian.org/debian-outreach/2018/07/msg00034.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-8-google-summer-of-code-with-debian-1d86226830b0


- report9:  https://lists.debian.org/debian-outreach/2018/07/msg00059.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-9-google-summer-of-code-with-debian-d288d8e2b1c1


- report10: https://lists.debian.org/debian-outreach/2018/07/msg00081.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-10-google-summer-of-code-with-debian-75eb8318826c


- report11: https://lists.debian.org/debian-outreach/2018/07/msg00107.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-11-google-summer-of-code-with-debian-e8a245ac15e6


- report12: https://lists.debian.org/debian-outreach/2018/08/msg00018.html
  - blog post - https://medium.com/@gajbhiyedeepanshu/week-12-google-summer-of-code-with-debian-784b0742fab9


## Other contributions
- Request to add 3 new packages and improve 1 package Debian edu Blends 
  - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=905006
  - https://salsa.debian.org/debian-edu/debian-edu/merge_requests/1
- Other blog posts
  - https://medium.com/@gajbhiyedeepanshu/solving-ltsp-client-not-able-to-login-issue-50384c98efbc
  - https://medium.com/@gajbhiyedeepanshu/building-custom-vagrant-box-e6a846b6baca